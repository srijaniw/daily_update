﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DailyUpdate.Infastructure.Migrations
{
    public partial class ChangesinModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignUsers_Projects_ProjectId",
                table: "AssignUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AssignUsers_Users_UserId",
                table: "AssignUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Updates_Users_CreatedById",
                table: "Updates");

            migrationBuilder.DropForeignKey(
                name: "FK_Updates_Projects_ProjectId",
                table: "Updates");

            migrationBuilder.DropIndex(
                name: "IX_Updates_CreatedById",
                table: "Updates");

            migrationBuilder.DropIndex(
                name: "IX_Updates_ProjectId",
                table: "Updates");

            migrationBuilder.DropIndex(
                name: "IX_AssignUsers_UserId",
                table: "AssignUsers");

            migrationBuilder.RenameColumn(
                name: "CreatedById",
                table: "Updates",
                newName: "CreatedBy");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProjectId",
                table: "Updates",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Updates",
                newName: "CreatedById");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProjectId",
                table: "Updates",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.CreateIndex(
                name: "IX_Updates_CreatedById",
                table: "Updates",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Updates_ProjectId",
                table: "Updates",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignUsers_UserId",
                table: "AssignUsers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignUsers_Projects_ProjectId",
                table: "AssignUsers",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssignUsers_Users_UserId",
                table: "AssignUsers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Updates_Users_CreatedById",
                table: "Updates",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Updates_Projects_ProjectId",
                table: "Updates",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
