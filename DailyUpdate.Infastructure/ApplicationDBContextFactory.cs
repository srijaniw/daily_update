﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace DailyUpdate.Infastructure
{
    public class ApplicationDBContextFactory : IDesignTimeDbContextFactory<DailyUpdateToolContext>
    {
        public DailyUpdateToolContext CreateDbContext(string[] args)
        {
            //Server=tcp:dailyupdate.database.windows.net,1433;Initial Catalog=DailyUpdate;Persist Security Info=False;User ID={your_username};Password={your_password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
            var builder = new DbContextOptionsBuilder<DailyUpdateToolContext>();
            builder.UseSqlServer("Server = tcp:dailyupdates.database.windows.net, 1433; Initial Catalog = DailyUpdate_DB; Persist Security Info = False; User ID = DailyUpdate; Password = Srijan@#001;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
                , optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(DailyUpdateToolContext).GetTypeInfo().Assembly.GetName().Name));

            return new DailyUpdateToolContext(builder.Options);
        }

       
    }
}
