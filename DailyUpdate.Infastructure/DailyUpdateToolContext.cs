﻿using DailyUpdate.Core.Model;
using JetBrains.Annotations;
using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;

namespace DailyUpdate.Infastructure
{
    public class DailyUpdateToolContext : DbContext
    {
        public DailyUpdateToolContext( DbContextOptions<DailyUpdateToolContext> options) : base(options)
        {
        }
       public virtual DbSet<Project> Projects { get; set; }
       public virtual DbSet<AssignUser> AssignUsers { get; set; }
       public virtual DbSet<Update> Updates { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssignUser>().HasKey(c => new { c.ProjectId,c.UserId });
            base.OnModelCreating(modelBuilder);
        }

    }
}
