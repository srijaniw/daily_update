﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DailyUpdate.Infastructure.Implementation
{
    public class AssignUserServices : IAssignUser
    {
        private readonly DailyUpdateToolContext _ctxt;
        public AssignUserServices(DailyUpdateToolContext ctxt)
        {
            _ctxt = ctxt;
        }
        public void CreateAssign(Core.Model.AssignUser assign)
        {
            _ctxt.Add(assign);
            _ctxt.SaveChanges();
        }

        public void DeleteAssign(Guid assignId)
        {
            var assign = _ctxt.AssignUsers.FirstOrDefault(a => a.Id == assignId);
            _ctxt.Remove(assign);
            _ctxt.SaveChanges();
        }

        public IEnumerable<AssignUserVM> GetAll()
        {
            var result = (from au in _ctxt.AssignUsers
                          join u in _ctxt.Users on au.UserId equals u.Id
                          join p in _ctxt.Projects on au.ProjectId equals p.Id
                          select new AssignUserVM()
                          {
                              Id = au.Id,
                              ProjectId=au.ProjectId,
                              UserId=au.UserId,
                              firstName=u.FirstName,
                              lastName=u.LastName,
                              projectName=p.Name
                                
                          });
            return result;
        }

        public void UpdateAssign(Core.Model.AssignUser assign)
        {
           var assignfrommDB = _ctxt.AssignUsers.FirstOrDefault(a => a.Id == assign.Id);
            assignfrommDB.UserId = assign.UserId;
            assignfrommDB.ProjectId = assign.ProjectId;
            _ctxt.Update(assignfrommDB);
            _ctxt.SaveChanges();
        }
    }
}
