﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DailyUpdate.Infastructure.Implementation
{
    public class UpdateService : IUpdate
    {
        private readonly DailyUpdateToolContext _ctx;
        public UpdateService(DailyUpdateToolContext ctx)
        {
            _ctx = ctx;
        }
        public void  AddUpdate(Update newUpdate)
        {
            
            _ctx.Add(newUpdate);
            _ctx.SaveChanges();

        }

        public void DeleteUpdate(Guid updateId)
        {
            var update = _ctx.Updates
                .FirstOrDefault(u => u.Id == updateId);
            if(update!=null)
            {
                _ctx.Remove(update);
                _ctx.SaveChanges();
            }
         
        }

        public void Update(Update newUpdate)
        {
            if (newUpdate != null)
            {
                var updatefromDB = _ctx.Updates.FirstOrDefault(u => u.Id == newUpdate.Id);
                updatefromDB.Description = newUpdate.Description;
                updatefromDB.ModifiedDate = DateTime.Now;
                _ctx.Update(updatefromDB);
                _ctx.SaveChanges();
            }
        }

        public IEnumerable<UpdateVM> GetAllUpdates()
        {
            var rslt = (from up in _ctx.Updates
                        join p in _ctx.Projects on up.ProjectId equals p.Id
                        join u in _ctx.Users on up.CreatedBy equals u.Id
                        select new UpdateVM()
                        {
                            Id = up.Id,
                            Description=up.Description,
                            CreatedDate=up.CreatedDate,
                            ModifiedDate=up.ModifiedDate,
                            CreatedBy=up.CreatedBy,
                            ProjectId=up.ProjectId,
                            firstName=u.FirstName,
                            lastName=u.LastName,
                            projectName=p.Name
                        });
            return rslt;
        }

        public IEnumerable<UpdateVM> GetUpdateByDate(DateTime date)
        {
            var rslt = (from up in _ctx.Updates
                        join p in _ctx.Projects on up.ProjectId equals p.Id
                        join u in _ctx.Users on up.CreatedBy equals u.Id
                        where up.ModifiedDate==date
                        select new UpdateVM()
                        {
                            Id = up.Id,
                            Description = up.Description,
                            CreatedDate = up.CreatedDate,
                            ModifiedDate = up.ModifiedDate,
                            CreatedBy = up.CreatedBy,
                            ProjectId = up.ProjectId,
                            firstName = u.FirstName,
                            lastName = u.LastName,
                            projectName = p.Name
                        });
            return rslt;

        }

        public IEnumerable<UpdateVM> GetUpdateByProject(Guid projectId)
        {
            var rslt = (from up in _ctx.Updates
                        join p in _ctx.Projects on up.ProjectId equals p.Id
                        join u in _ctx.Users on up.CreatedBy equals u.Id
                        where up.ProjectId==projectId
                        select new UpdateVM()
                        {
                            Id = up.Id,
                            Description = up.Description,
                            CreatedDate = up.CreatedDate,
                            ModifiedDate = up.ModifiedDate,
                            CreatedBy = up.CreatedBy,
                            ProjectId = up.ProjectId,
                            firstName = u.FirstName,
                            lastName = u.LastName,
                            projectName = p.Name
                        });
            return rslt;
           
        }

        public IEnumerable<UpdateVM> GetUpdateByUserId(Guid userId)
        {
            var rslt = (from up in _ctx.Updates
                        join p in _ctx.Projects on up.ProjectId equals p.Id
                        join u in _ctx.Users on up.CreatedBy equals u.Id
                        where up.CreatedBy==userId
                        select new UpdateVM()
                        {
                            Id = up.Id,
                            Description = up.Description,
                            CreatedDate = up.CreatedDate,
                            date = up.ModifiedDate,
                            CreatedBy = up.CreatedBy,
                            ProjectId = up.ProjectId,
                            firstName = u.FirstName,
                            lastName = u.LastName, 
                            projectName = p.Name
                        });
            return rslt;
        }
 
    }

  
    
}
