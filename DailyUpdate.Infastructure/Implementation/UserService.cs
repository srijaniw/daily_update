﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DailyUpdate.Infastructure.Implementation
{

    public class UserService : IUser
    {
        private readonly DailyUpdateToolContext _ctxt;
      
        public UserService(DailyUpdateToolContext ctxt)
        {
            _ctxt = ctxt;
        }



        public IEnumerable<UserVM> GetUsersByProject(Guid projectID)
        {
            var rslt = (from u in _ctxt.Users
                        join au in _ctxt.AssignUsers on u.Id equals au.UserId
                        join p in _ctxt.Projects on au.ProjectId equals p.Id
                        where p.Id == projectID
                        select new UserVM()
                        {
                            Id = u.Id,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            UserName = u.UserName,
                            ProjectName = p.Name,     
                        }
                      );
            return rslt;
        }

        public UserVM GetUserByUserName(string userName)
        {
            //var rslt = (from u in _ctxt.Users
            //            join au in _ctxt.AssignUsers on u.Id equals au.UserId
            //            join p in _ctxt.Projects on au.ProjectId equals p.Id
            //            where u.UserName == userName
            //            select new UserVM()
            //            {
            //                Id = u.Id,
            //                FirstName = u.FirstName,
            //                LastName = u.LastName,
            //                UserName = u.UserName,
            //                ProjectName = p.Name
            //            }
            //          ).FirstOrDefault();

            var result = from u in _ctxt.Users.Where(x => x.UserName == userName)
                          select new UserVM {
                              Id = u.Id,
                              FirstName = u.FirstName,
                              LastName = u.LastName,
                              UserName = u.UserName,
                              Email=u.Email
                              
                          };

            return result.FirstOrDefault();
        }



        public void CreateUser(User userToAdd)
        {
      
            _ctxt.Add(userToAdd);
            _ctxt.SaveChanges();
        }

        public void UpdateUser(User userToUpdate)
        {
            var userFromDB = _ctxt.Users.FirstOrDefault(u => u.Id == userToUpdate.Id);
            userFromDB.FirstName = userToUpdate.FirstName;
            userToUpdate.LastName = userToUpdate.LastName;
            userToUpdate.UserName = userToUpdate.UserName;
            userToUpdate.Password = userToUpdate.Password;
            userToUpdate.Email = userToUpdate.Email;
            userToUpdate.Role = (Core.Enum.Role)2;
            userToUpdate.Status = (Core.Enum.UserStatus)1;
            _ctxt.Update(userFromDB);
            _ctxt.SaveChanges();
        }

        public void DeleteUser(Guid userId)
        {
            var userFromDB = _ctxt.Users.FirstOrDefault(u => u.Id == userId);
            
            _ctxt.Remove(userFromDB);
            _ctxt.SaveChanges();
        }

        public IEnumerable<User> GetAllUser()
        {
            var res = _ctxt.Users.AsEnumerable().ToList();
           
            return res;
        }

        public User CheckUser(string email, string password)
        {
            var result = _ctxt.Users.Where(u => u.Email == email)
                .Where(u => u.Password == password).SingleOrDefault();
            return result;        
        }
    }
       
}

