﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DailyUpdate.Infastructure.Implementation
{
    public class Projectservice : IProject
    {
        private readonly DailyUpdateToolContext _ctxt;
        public Projectservice(DailyUpdateToolContext ctxt)
        {
            _ctxt = ctxt;
        }
        public void AddEntity(Project entityToAdd)
        {
            _ctxt.Add(entityToAdd);
            _ctxt.SaveChanges();
        }

        public void DeleteEntity(Guid projectId)
        {
            var project = _ctxt.Projects
                  .FirstOrDefault(p => p.Id == projectId);
            _ctxt.Remove(project);
            _ctxt.SaveChanges();

        }

        public IEnumerable<Project> GetAll()
        {
            return _ctxt.Projects;
        }

        public Project GetEntityById(Guid projectId)
        {
            return _ctxt.Projects.
                 FirstOrDefault(p => p.Id == projectId);
        }

        public IEnumerable<Core.Model.Project> GetProjectByProjectName(string project)
        {
            return _ctxt.Projects.
                 Where(p => p.Name == project);
        }

        public Project GetProjectbyUser(Guid userId)
        {
            var result = (from p in _ctxt.Projects
                        join au in _ctxt.AssignUsers on p.Id equals au.ProjectId
                        join u in _ctxt.Users on au.UserId equals u.Id
                        where u.Id==userId
                        select new Project
                        {Id=p.Id,
                        Name=p.Name
                        }).SingleOrDefault();
            return result;
        }

        public void UpdateEntity(Project entityToUpdate)
        {
            var projectFromDatbase = _ctxt.Projects.FirstOrDefault(p => p.Id == entityToUpdate.Id);
            projectFromDatbase.Name = entityToUpdate.Name;
            _ctxt.Update(projectFromDatbase);
            _ctxt.SaveChanges();
        }
    }
}
