﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace DailyUpdate.API.Controllers
{
    [EnableCors("DailyUpdatePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateController : ControllerBase
    {
        private readonly IUpdate _updateService;
       private readonly ILogger<UpdateController> _logger;
        public UpdateController(IUpdate updateService,ILogger<UpdateController> logger)
        {
            _updateService = updateService;
            _logger = logger;
        }

        [HttpGet]
        
        public ActionResult<IEnumerable<Update>> GetAllUpdate()
        {
            try
            {
                return Ok(_updateService.GetAllUpdates());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get All Update:{ex}");
                return BadRequest(); 
            } 
        }

        [Route("UpdatebyDate/{date}")]
        [HttpGet]
        public ActionResult<IEnumerable<Update>> GetUpdatebyDate([FromRoute] DateTime date)
        {
            try
            {
                return Ok(_updateService.GetUpdateByDate(date));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Update by Date:{ex}");
                return BadRequest();
            }
            
        }

        [Route("UpdatebyProject/{projectId}")]
        [HttpGet]
        public ActionResult<IEnumerable<Update>> GetUpdatebyProject([FromRoute] Guid projectId)
        {
            try
            {
                return Ok(_updateService.GetUpdateByProject(projectId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Update by Project:{ex}");
                return BadRequest();
            }
        }

        [Route("UpdatebyUserId/{userId}")]
        [HttpGet]
        public ActionResult<IEnumerable<Update>> GetUpdatebyUser([FromRoute] Guid userId)
        {
            try
            {
                return Ok(_updateService.GetUpdateByUserId(userId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Update by User:{ex}");
                return BadRequest();
            }
        }

     
        [HttpPost]
        public ActionResult CreateUdpate([FromBody]Update update)
        {
            try
            {
                _updateService.AddUpdate(update);
                return  Ok("Created Update");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Add update:{ex}");
                return BadRequest();
            }
          
        }
        
        [Route("Delete/{updateId}")]
        [HttpDelete]
        public IActionResult DeleteUpdate([FromRoute] Guid updateId)
        {
            try
            {
                _updateService.DeleteUpdate(updateId);
                return Ok("Deleted Update");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Delete update:{ex}");
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult EditUpdate([FromBody] Update entitytoUpdate)
        {
            try
            {
                _updateService.Update(entitytoUpdate);
                return Ok("Edited Update");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Edit update:{ex}");
                return BadRequest();
            }
        }
    }
}