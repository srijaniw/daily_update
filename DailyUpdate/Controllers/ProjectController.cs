﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace DailyUpdate.API.Controllers
{
    [EnableCors("DailyUpdatePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProject _projectServcie;
        private readonly ILogger<UpdateController> _logger;
        public ProjectController(IProject projectService, ILogger<UpdateController> logger)
        {
            _projectServcie = projectService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Project>> GetAllProject()
        {
            try
            {
                return Ok(_projectServcie.GetAll());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get All Project:{ex}");
                return BadRequest();
            }
        }
        [HttpPost]
        public ActionResult CreateProject([FromBody]Project project)
        {
            try
            {
                _projectServcie.AddEntity(project);
                return Ok("Created Project");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Add Project:{ex}");
                return BadRequest();
            }

        }

        [Route("Delete/{projectId}")]
        [HttpDelete]
        public IActionResult DeleteProject([FromRoute] Guid projectId)
        {
            try
            {
                _projectServcie.DeleteEntity(projectId);
                return Ok("Deleted Project");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Delete Project:{ex}");
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult EditProject([FromBody] Project entitytoUpdate)
        {
            try
            {
                _projectServcie.UpdateEntity(entitytoUpdate);
                return Ok("Edited Project");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Edit Project:{ex}");
                return BadRequest();
            }
        }

        [Route("ProjectbyName/{projectName}")]
        [HttpGet]
        public ActionResult<IEnumerable<Project>> GetProjectbyName([FromRoute] string projectName)
        {
            try
            {
                return Ok(_projectServcie.GetProjectByProjectName(projectName));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Project by Name:{ex}");
                return BadRequest();
            }
        }

        [Route("ProjectbyId/{projectId}")]
        [HttpGet]
        public ActionResult<IEnumerable<Project>> GetProjectbyId([FromRoute] Guid projectId)
        {
            try
            {
                return Ok(_projectServcie.GetEntityById(projectId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Project by Id:{ex}");
                return BadRequest();
            }
        }

        [Route("ProjectbyUser/{userId}")]
        [HttpGet]
        public ActionResult<IEnumerable<Project>> GetProjectbyUser([FromRoute] Guid userId)
        {
            try
            {
                return Ok(_projectServcie.GetProjectbyUser(userId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get Project by Id:{ex}");
                return BadRequest();
            }
        }
    }
}