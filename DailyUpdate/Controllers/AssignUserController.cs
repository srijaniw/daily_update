﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace DailyUpdate.API.Controllers
{
    [EnableCors("DailyUpdatePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AssignUserController : ControllerBase
    {
        private readonly IAssignUser _assignUserService;
        private readonly ILogger<UpdateController> _logger;
        public AssignUserController(IAssignUser assignUserService, ILogger<UpdateController> logger)
        {
            _assignUserService = assignUserService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AssignUser>> GetAll()
        {
            try
            {
                return Ok(_assignUserService.GetAll());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get All AssignUser:{ex}");
                return BadRequest();
            }
        }

        [HttpPost]
        public ActionResult CreateAssignUser([FromBody]AssignUser assign)
        {
            try
            {
                _assignUserService.CreateAssign(assign);
                return Ok("Created Assign");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Add Assign:{ex}");
                return BadRequest();
            }

        }

        [Route("Delete/{assignId}")]
        [HttpDelete]
        public IActionResult DeleteAssign([FromRoute] Guid assignId)
        {
            try
            {
                _assignUserService.DeleteAssign(assignId);
                return Ok("Deleted Assign");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Delete Assign:{ex}");
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult EditAssign([FromBody] AssignUser assign)
        {
            try
            {
                _assignUserService.UpdateAssign(assign);
                return Ok("Edited Assign");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Edit Assign:{ex}");
                return BadRequest();
            }
        }
    }
}