﻿using DailyUpdate.Core.Interfaces;
using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace DailyUpdate.API.Controllers
{
    [EnableCors("DailyUpdatePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser _userService;
        private readonly ILogger<UpdateController> _logger;
        public UserController(IUser userService, ILogger<UpdateController> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserVM>> GetAllUser()
        {
            try
            {
                var result = _userService.GetAllUser();
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get All User:{ex}");
                return BadRequest();
            }
        }

        [Route("UserbyProject/{projectId}")]
        [HttpGet]
        public ActionResult<IEnumerable<UserVM>> GetUpdatebyProject([FromRoute] Guid projectId)
        {
            try
            {
                return Ok(_userService.GetUsersByProject(projectId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get User by Project:{ex}");
                return BadRequest();
            }
        }

        [Route("UserbyUsername/{userName}")]
        [HttpGet]
        public IActionResult GetUpdatebyuserName([FromRoute] string userName)
        {
            try
            {
                var result = _userService.GetUserByUserName(userName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Get User by UserName:{ex}");
                return BadRequest();
            }
        }

        [HttpPost]
        public ActionResult CreateUser([FromBody]User user)
        {
            try
            {
                _userService.CreateUser(user);
                return Ok("Created User");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Add User:{ex}");
                return BadRequest();
            }
        }

        [Route("Delete/{userId}")]
        [HttpDelete]
        public IActionResult DeleteUser([FromRoute] Guid userId)
        {
            try
            {
                _userService.DeleteUser(userId);
                return Ok("Deleted User");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Delete User:{ex}");
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult EditUser([FromBody] User entitytoUpdate)
        {
            try
            {
                _userService.UpdateUser(entitytoUpdate);
                return Ok("Edited User");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Edit User:{ex}");
                return BadRequest();
            }
        }

        [Route("Login")]
        [HttpPost]
        public ActionResult CheckUser([FromBody] User user)
        {
            try
            {
                var result=_userService.CheckUser(user.Email, user.Password);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to Login User:{ex}");
                return BadRequest();
            }
        }
    }
}