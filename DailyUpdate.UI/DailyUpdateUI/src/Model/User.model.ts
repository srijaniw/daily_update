export class User
{
    id:string;
    firstName:string;
    lastName:string;
    userName:string;
    email:string;
    password:string;
    role:number;
    status:number;
}

 export enum role{
     Admin=1,
     User
 }
 export enum status
 {
     Active=1,
     Inactive
 }