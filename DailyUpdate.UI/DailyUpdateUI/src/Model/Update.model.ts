export class Update
{
    id:number;
    description:string;
    createdDate:Date;
    modifiedDate:Date;
    createdBy:number;
    project:number;
    firstName:string;
    lastName:string;
    projectname:string;
}