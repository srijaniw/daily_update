import { Component, OnInit } from '@angular/core';
import { UpdateService } from '../../Services/update.service';
import { UserService } from '../../Services/user.service';
import { ProjectService } from '../../Services/project.service';
import { User } from '../../Model/User.model';
import { Project } from '../../Model/Project.model';
import { Update } from '../../Model/Update.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  public users:User[];
  public projects:Project[];
  public updates:Update[];
  constructor(private updateService:UpdateService,private userService:UserService,private projectService:ProjectService,private datepipe:DatePipe) { }

  ngOnInit() {
    this.userService.GetUserList().subscribe((data=>{this.users=data
    console.log(data)}));
    this.updateService.GetUpdateList().subscribe((data=>{this.updates=data
      console.log(data)}));
    this.projectService.getProjectList().subscribe((data=>{this.projects=data
    console.log(data)}));
  }
  selectProject(event1:any)
  {
    this.updateService.GetUpdatebyProject(event1.target.value).subscribe((data)=>{this.updates=data
    console.log(data);});
  }
  selecDate(event2:any)
  {
    var date=this.datepipe.transform(event2.target.value,"yyyy-MM-dd");
    this.updateService.GetUpdatebyDate(date).subscribe((data)=>{this.updates=data
      console.log(data);});
  }
  selectUser(event3:any)
  { 
    this.updateService.GetUpdatebyUser(event3.target.value).subscribe((data)=>{this.updates=data
      console.log(data);});
  }
}
