import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

 constructor(private router:Router) { }

  ngOnInit() { 
  }

  showProject()
  {
    this.router.navigate(['project']);
  }
  showUser()
  {
    this.router.navigate(['user']);
  }
  showUpdate()
  {
    this.router.navigate(['update']);
  }
  onLogout()
  {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
