import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service';
import { User } from '../../Model/User.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public users:User[];

User:User=new User();
//  User:any={
// id:"00000000-0000-0000-0000-000000000000",
// firstName:"",
// lastName:"",
// userName:"",
// email:"",
// role:2,
// status:1
//   }
  constructor(private userService:UserService) { }

  ngOnInit() {
    this.loadUserData();
  }
  loadUserData()
  {
    this.userService.GetUserList().subscribe((data=>{this.users=data
      console.log(data)}));
  }
  onAdd(User){
    if(User.id==null)
    {
      console.log(User);
      this.userService.CreateUser(User).subscribe((data)=>{},()=>{this.loadUserData(),alert("User Created")});
    }
    else{
    console.log(User);
   this.userService.UpdateUser(User).subscribe((data)=>{},()=>{this.loadUserData(),alert("User Edited")});
    }
  }
  onUserEdit(u){
    this.userService.GetUserbyuserName(u.userName).subscribe((data)=>{this.User=data
      console.log(data)});
  }
  ondeletePlayer(id){
    console.log(id);
    this.userService.DeleteUser(id).subscribe((data)=>{},()=>{this.loadUserData(),alert("User Deleted")});
  }
}
