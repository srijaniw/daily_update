import { Component, OnInit } from '@angular/core';
import { User } from '../../Model/User.model';
import { Project } from '../../Model/Project.model';
import { ProjectService } from '../../Services/project.service';
import { UserService } from '../../Services/user.service';

import { AssignUserService } from '../../Services/assign-user.service';
import { AssignUser } from '../../Model/AssignUser.model';
import { Body } from '@angular/http/src/body';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  public users:User[];
  public assignUsers:AssignUser[];
  AssignUser:AssignUser=new AssignUser();
  public projects:Project[];
  Project:Project=new Project();
  pId:string;
  uId:string;
  
  constructor(private userService:UserService,private projectService:ProjectService,private assignuserService:AssignUserService) { }

  ngOnInit() {
    this.userService.GetUserList().subscribe((data=>{this.users=data
      console.log(data)}));
    //  this.projectService.getProjectList().subscribe((data=>{this.projects=data
    //   console.log(data)}));  
    this.loadProjectData();
      this.loadAssignData();
  }

  loadProjectData()
  {
    this.projectService.getProjectList().subscribe((data=>{this.projects=data
      console.log(data)}));  
  }

  loadAssignData()
  {
  this.assignuserService.getAssignList().subscribe((data)=>{this.assignUsers=data
  console.log(data);});
  }

 onSubmit(Project){
 if(Project.id==null)
 {
  console.log(Project);
  this.projectService.CreateProject(Project).subscribe((data)=>{},
  ()=>{this.loadProjectData(), alert("Created Project")});  
  }
}
  
  onLoadProject(p){
    console.log(p);
    this.Project.id=p.id;
    this.Project.name=p.name;
  }

  onDeleteProject(p){
    console.log(p.id);
    this.projectService.DeleteAssign(p.id).subscribe((data)=>{},
    ()=>{this.loadProjectData(), alert("Created Project")}); 
  }

  selectUser(event1:any){this.uId=event1.target.value;}
  selectProject(event2:any){this.pId=event2.target.value}
  onAssign()
  {
    this.AssignUser.projectId=this.pId;
    this.AssignUser.userId=this.uId;
    console.log(this.pId);
    console.log(this.uId);
    
    this.assignuserService.Assign(AssignUser).subscribe((data)=>{},
     ()=>{this.loadProjectData(), alert("Assigned Project")});
  }


  onDeleteAssign(au)
  {
    console.log(au.id);
    this.assignuserService.DeleteAssign(au.id).subscribe((data)=>{},
   ()=>{this.loadProjectData(), alert("Deleted Assigned")});
  }
}
