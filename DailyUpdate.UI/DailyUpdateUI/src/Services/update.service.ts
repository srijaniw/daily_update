import { Injectable } from '@angular/core';
import {  
  HttpClient, HttpHeaders 
} from '@angular/common/http';  

import {  
  Observable  
} from 'rxjs'; 
import { Update } from '../Model/Update.model';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {
  

  constructor(private http:HttpClient) { }

  GetUpdateList():Observable<Update[]>
  {
    return this.http.get<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update');
  }
 
  CreateUpdate(update)
  {
    return this.http.post<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update',update);
  }

  DeleteUpdate(updateId)
  {
    return this.http.delete<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update/Delete/'+updateId);
  }

  EditUpdate(editedUpdate)
  {
    return this.http.put<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update',editedUpdate);
  }

  GetUpdatebyUser(userId):Observable<Update[]>
  {
    return this.http.get<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update/UpdatebyUserId/'+userId);
  }

  GetUpdatebyProject(projectId):Observable<Update[]>
  {
    return this.http.get<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update/UpdatebyProject/'+projectId);
  }

  GetUpdatebyDate(date):Observable<Update[]>
  {
    return this.http.get<Update[]>('https://dailyupdateapi.azurewebsites.net/api/Update/UpdatebyDate/'+date);
  }
}
