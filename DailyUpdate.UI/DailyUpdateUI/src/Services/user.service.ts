import { Injectable } from '@angular/core';
import {  
  HttpClient, HttpHeaders 
} from '@angular/common/http';  

import {  
  Observable  
} from 'rxjs';
import { User } from '../Model/User.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  GetUserList():Observable<User[]>
  {
    return this.http.get<User[]>('https://dailyupdateapi.azurewebsites.net/api/User');
  }

  CreateUser(user)
  {
    debugger;
    return this.http.post<User[]>('https://dailyupdateapi.azurewebsites.net/api/User',user);
  }

  DeleteUser(userId)
  {
    return this.http.delete<User[]>('https://dailyupdateapi.azurewebsites.net/api/User/Delete/'+userId);
  }

  UpdateUser(updateUser)
  {
    console.log(updateUser);
    return this.http.put<User>('https://dailyupdateapi.azurewebsites.net/api/User',updateUser);
  }

  GetUserbyuserName(userName):Observable<User>
  {
    return this.http.get<User>('https://dailyupdateapi.azurewebsites.net/api/User/UserbyUsername/'+userName);
  }
  
  GetUserbyprojectId(projectId):Observable<User[]>
  {
    return this.http.get<User[]>('https://dailyupdateapi.azurewebsites.net/api/User/UserbyProject/'+projectId);
  }

  CheckUser(user)
  {
    return this.http.post<User>('https://dailyupdateapi.azurewebsites.net/api/User/Login',user);
  }
}