import { Injectable } from '@angular/core';
import {  
  HttpClient, HttpHeaders 
} from '@angular/common/http';  

import {  
  Observable  
} from 'rxjs';  
import { AssignUser } from '../Model/AssignUser.model';


@Injectable({
  providedIn: 'root'
})
export class AssignUserService {

  constructor(private http:HttpClient) { }
  getAssignList():Observable<AssignUser[]>
  {
    return this.http.get<AssignUser[]>('https://dailyupdateapi.azurewebsites.net/api/AssignUser');
  }

  Assign(assignUser)
  {
    return this.http.post<AssignUser[]>('https://dailyupdateapi.azurewebsites.net/api/AssignUser',assignUser);
  }

  DeleteAssign(assignId)
  {
    return this.http.delete<AssignUser[]>('https://dailyupdateapi.azurewebsites.net/api/AssignUser/Delete/'+assignId);
  }
}
