import { TestBed, inject } from '@angular/core/testing';

import { AssignUserService } from './assign-user.service';

describe('AssignUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssignUserService]
    });
  });

  it('should be created', inject([AssignUserService], (service: AssignUserService) => {
    expect(service).toBeTruthy();
  }));
});
