import { Injectable } from '@angular/core';
import {  
  HttpClient, HttpHeaders 
} from '@angular/common/http';  

import {  
  Observable  
} from 'rxjs';  
import { Project } from '../Model/Project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http:HttpClient) { }
  getProjectList():Observable<Project[]>
  {
    return this.http.get<Project[]>('https://dailyupdateapi.azurewebsites.net/api/Project');
  }

  CreateProject(project)
  {
    console.log(project);
    return this.http.post<Project[]>('https://dailyupdateapi.azurewebsites.net/api/Project',project);
  }

  DeleteAssign(projectId)
  {
    return this.http.delete<Project[]>('https://dailyupdateapi.azurewebsites.net/api/Project/Delete/'+projectId);
  }

  UpdateProject(updateProject)
  {
    return this.http.put<Project[]>('https://dailyupdateapi.azurewebsites.net/api/Project',updateProject);
  }
  getProjectbyUser(uId)
  {
    return this.http.get<Project[]>('https://dailyupdateapi.azurewebsites.net/api/Project/ProjectbyUser/'+uId);
  }
}
