import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  User:any={
    email:"",
    password:""
  }
  constructor(private router:Router,private userService:UserService) { }

  ngOnInit() {
  }
  onLogin(user)
  {
    console.log("user",user);
    this.userService.CheckUser(user).subscribe((data)=>{
      console.log(data);
      localStorage.setItem('Token',JSON.stringify({ data }));
      if(user.email=="admin" && user.password=="admin")
      {
        this.router.navigate(['navbar/update']);
      }
      else
      {
        this.router.navigate(['fnavbar/f-update']);
      }
    });
  }
}
