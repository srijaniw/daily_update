import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import {HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { DashboardComponent } from '.././BackEnd/dashboard/dashboard.component';
import { ProjectComponent } from '.././BackEnd/project/project.component';
import { UpdateComponent } from '.././BackEnd/update/update.component';
import { FUpdateComponent } from '.././FrontEnd/f-update/f-update.component';
import { FormsModule } from '@angular/forms';
import { FnavbarComponent } from '.././FrontEnd/fnavbar/fnavbar.component';
import { NavbarComponent } from '.././BackEnd/navbar/navbar.component';
import { UserComponent } from '.././BackEnd/user/user.component';
import { LoginComponent } from '../login/login.component';
import { PersonalDetailsComponent } from '.././FrontEnd/personal-details/personal-details.component';
import { Routes, RouterModule} from '@angular/router';
import { DatePipe } from '@angular/common';

const appRoutes:Routes=[
{path:"",component:LoginComponent},
{path:'navbar',component:NavbarComponent,children:
[
{path:'update',component:UpdateComponent},
{path:'project',component:ProjectComponent},
{path:'user',component:UserComponent}
]},
{path:'fnavbar',component:FnavbarComponent,children:[
{path:'f-update',component:FUpdateComponent},
{path:'personal-details',component:PersonalDetailsComponent}
]}
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProjectComponent,
    UpdateComponent,
    FUpdateComponent,
    FnavbarComponent,
    NavbarComponent,
    UserComponent,
    LoginComponent,
    PersonalDetailsComponent
  ],
  imports: [

    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
   RouterModule.forRoot(appRoutes)
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
