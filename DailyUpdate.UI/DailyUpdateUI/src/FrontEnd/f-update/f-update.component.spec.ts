import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FUpdateComponent } from './f-update.component';

describe('FUpdateComponent', () => {
  let component: FUpdateComponent;
  let fixture: ComponentFixture<FUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
