import { Component, OnInit } from '@angular/core';
import { UpdateService } from '../../Services/update.service';
import { Update } from '../../Model/Update.model';
import { ProjectService } from '../../Services/project.service';
import { Project } from '../../Model/Project.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-f-update',
  templateUrl: './f-update.component.html',
  styleUrls: ['./f-update.component.css']
})
export class FUpdateComponent implements OnInit {
updates:Update[];
projects:Project[];
Project:Project=new Project();
Update:Update=new Update();
  constructor(private updateService:UpdateService,private projectService:ProjectService,public datepipe: DatePipe) { }

  ngOnInit() {
    
    this.loadUpdateData();
  }
  // var id=sessionStorage.getItem();
loadUpdateData()
{
  let array=JSON.parse(localStorage.getItem('Token'));
  let id=array.data.id;
  
  this.updateService.GetUpdatebyUser(id).subscribe((data)=>{this.updates=data
  console.log(data);});
  this.projectService.getProjectbyUser(id).subscribe((data)=>{this.projects=data
    console.log(data);
  }); 
}
onPost(description)
{
  let array=JSON.parse(localStorage.getItem('Token'));
  let id=array.data.id;

  let myDate = new Date(); 
  console.log(this.datepipe.transform(myDate, 'yyyy-mm-dd'));
  this.Update.description=description;
   
  this.Update.createdDate=myDate;
  this.Update.modifiedDate=myDate;
  this.Update.createdBy=id;
   //this.Update.project=
}
}
