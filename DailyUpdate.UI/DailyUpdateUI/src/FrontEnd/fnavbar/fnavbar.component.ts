import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fnavbar',
  templateUrl: './fnavbar.component.html',
  styleUrls: ['./fnavbar.component.css']
})
export class FnavbarComponent implements OnInit {

  constructor(private router:Router) { 
    this.router.navigate(['/fnavbar/f-update']);
  }

  ngOnInit() {
  }
  onLogout()
  {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
