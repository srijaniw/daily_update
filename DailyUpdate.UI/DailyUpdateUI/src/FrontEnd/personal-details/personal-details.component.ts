import { Component, OnInit } from '@angular/core';
import { User } from '../../Model/User.model';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit {
public User:User=new User();
  constructor() { }

  ngOnInit() {
  }

}
