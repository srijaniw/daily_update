﻿using DailyUpdate.Core.Model;
using DailyUpdate.Infastructure;
using DailyUpdate.Infastructure.Implementation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace DailyUpdate.Infstructure.Test
{
    public class UpdateServiceTest
    {

        private readonly Update _fakeUpdate;
        private readonly DbContextOptions<DailyUpdateToolContext> fakeDbContext;
        public UpdateServiceTest()
        {
            fakeDbContext = new DbContextOptionsBuilder<DailyUpdateToolContext>().UseInMemoryDatabase(databaseName: "UsingTestDatabase").Options;

            _fakeUpdate = new Update
            {
                Id = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A"),
                Description = "This is Today's Update",
                CreatedDate = Convert.ToDateTime("2018-08-23"),
                CreatedBy  = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A"),
               ProjectId = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A")   
            };
        }
        [Fact]
        public void GetUpdatebyDate_Should_Get_Updates_From_Date()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeUpdate);
                ctxt.SaveChanges();

                //Act
                var updateService = new UpdateService(ctxt);
                var data = updateService.GetUpdateByDate(Convert.ToDateTime("2018-08-23")).ToList();

                //Assert
                Assert.Equal("This is Today's Update", data[0].Description);

            }
        }

        [Fact]
        public void GetUpdatebyProject_Should_Get_Updates_From_Project()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeUpdate);
                ctxt.SaveChanges();

                //Act
                var updateService = new UpdateService(ctxt);
                var pId = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A");
                var data = updateService.GetUpdateByProject(pId).ToList();

                //Assert
                Assert.Single(data);
                Assert.Equal("This is Today's Update", data[0].Description);
            }
        }

        [Fact]
        public void GetUpdatebyUser_Should_Get_Updates_From_User()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeUpdate);
                ctxt.SaveChanges();

                //Act
                var updateService = new UpdateService(ctxt);
                var uId = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A");
                var data = updateService.GetUpdateByUserId(uId).ToList();

                //Assert
                Assert.Single(data);
                Assert.Equal("This is Today's Update", data[0].Description);

            }
        }

        [Fact]
        public void CreateUpdate_Should_Create_Update()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Assert
                ctxt.Database.EnsureDeleted();
               

                //Act
                var updateService = new UpdateService(ctxt);
                updateService.AddUpdate(_fakeUpdate);
                var data = updateService.GetUpdateByProject(_fakeUpdate.Id).ToList();

                //Assert
                Assert.Equal("This is Today's Update", data[0].Description);
            }
        }

        [Fact]
        public void DeleteUpdate_Should_Delete_Update()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Assert
               

                //Act
                var updateService = new UpdateService(ctxt);
                updateService.DeleteUpdate(_fakeUpdate.Id);
                var data = updateService.GetUpdateByProject(_fakeUpdate.Id);

                //Assert
                Assert.Empty(data);

            }
        }

        [Fact]
        public void GetAllUpdates_Should_Get_All_Updates()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeUpdate);
                ctxt.SaveChanges();

                //Act
                var updateService = new UpdateService(ctxt);
                var pId = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A");
                var data = updateService.GetAllUpdates().ToList();

                //Assert
                Assert.Single(data);
                Assert.Equal("This is Today's Update", data[0].Description);
            }
        }
    }
}



