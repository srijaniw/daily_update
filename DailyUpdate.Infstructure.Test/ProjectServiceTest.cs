﻿using DailyUpdate.Core.Model;
using DailyUpdate.Infastructure;
using DailyUpdate.Infastructure.Implementation;
using Microsoft.EntityFrameworkCore;
using System;
using Xunit;

namespace DailyUpdate.Infstructure.Test
{
    public  class ProjectServiceTest
    {

        private readonly Project _fakeProject;
        private readonly DbContextOptions<DailyUpdateToolContext> fakeDbContext;
        public ProjectServiceTest()
        {
            fakeDbContext = new DbContextOptionsBuilder<DailyUpdateToolContext>().UseInMemoryDatabase(databaseName: "UsingTestDatabase").Options;
            _fakeProject = new Project()
            {
                Id = Guid.Parse("4FF640AE-D816-453A-9148-E98268015A4A"),
                Name = "Project A"
            };
        }

        [Fact]
        public void GetProjectbyprojectName_Should_Get_Project_From_projectName()
        {
            using (var ctxt=new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeProject);
                ctxt.SaveChanges();

                //Act
                var projectServices = new Projectservice(ctxt);
                var data = projectServices.GetProjectByProjectName("Project A");

                //Arrange
                Assert.Single(data);
               
            }
        }

        [Fact]
        public void GetAllProject_Should_Get_All_Project()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeProject);
                ctxt.SaveChanges();

                //Act
                var projectServices = new Projectservice(ctxt);
                var data = projectServices.GetAll();

                //Arrange
                Assert.Single(data);

            }
        }

        [Fact]
        public void AddProject_Should_Create_Project()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();

                //Act
                var projectServices = new Projectservice(ctxt);
                projectServices.AddEntity(_fakeProject);
                var data = projectServices.GetAll();

                //Arrange
                Assert.Single(data);
            }
        }

        [Fact]
        public void DeleteProject_Should_Delete_Project()
        {
            using (var ctxt = new DailyUpdateToolContext(fakeDbContext))
            {
                //Arrange
                ctxt.Database.EnsureDeleted();
                ctxt.Add(_fakeProject);
                ctxt.SaveChanges();

                //Act
                var projectServices = new Projectservice(ctxt);
                projectServices.DeleteEntity(_fakeProject.Id);
                var data= projectServices.GetEntityById(_fakeProject.Id);

                //Assert
                Assert.Null(data.Name);
            }
        }

    }
}
