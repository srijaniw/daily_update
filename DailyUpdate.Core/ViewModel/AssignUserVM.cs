﻿using DailyUpdate.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.ViewModel
{
public class AssignUserVM:AssignUser
    {
        public string projectName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

    }
}
