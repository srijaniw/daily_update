﻿using DailyUpdate.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.ViewModel
{
   public class UpdateVM:Update
    {
       
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string projectName { get; set; }
        public DateTime date { get; set; }

    }
}
