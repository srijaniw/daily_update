﻿using DailyUpdate.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.ViewModel
{
   public class UserVM:User
    {
        public string ProjectName { get; set; }
    }
}
