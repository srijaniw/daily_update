﻿using DailyUpdate.Core.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DailyUpdate.Core.Model
{
   public class User:DataEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string UserName { get; set; }
        [MaxLength(255)]
        public string Email { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public UserStatus Status { get; set; }
    }
}
