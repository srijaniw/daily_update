﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DailyUpdate.Core.Model
{
    public class AssignUser : DataEntity
    {   
        [ForeignKey("ProjectId")]
        public Guid ProjectId { get; set; }
       
        [ForeignKey("UserId")]
        public Guid UserId { get; set; }
       
    }
}
