﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DailyUpdate.Core.Model
{
    public  class Update:DataEntity
    {
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public Guid  CreatedBy { get; set; }
        [ForeignKey("ProjectId")]
        public Guid  ProjectId { get; set; }
    }
}
