﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DailyUpdate.Core.Model
{
    public class Project : DataEntity
    {
        [StringLength(200)]
        public string Name { get; set; }
    }
}
