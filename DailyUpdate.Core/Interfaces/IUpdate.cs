﻿using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.Interfaces
{
   public interface IUpdate
    {
        IEnumerable<UpdateVM> GetAllUpdates();
        IEnumerable<UpdateVM> GetUpdateByUserId(Guid userId);
        IEnumerable<UpdateVM> GetUpdateByProject(Guid projectId);
        IEnumerable<UpdateVM> GetUpdateByDate(DateTime date);
        void AddUpdate(Update update);
        void DeleteUpdate(Guid Id);
        void Update(Update update);
    }
}
