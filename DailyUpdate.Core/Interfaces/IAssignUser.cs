﻿using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;

namespace DailyUpdate.Core.Interfaces
{
    public interface IAssignUser
    {
        IEnumerable<AssignUserVM> GetAll();
        void CreateAssign(AssignUser assign);
        void UpdateAssign(AssignUser assign);
        void DeleteAssign(Guid assignId);
    }
}
