﻿using DailyUpdate.Core.Model;
using DailyUpdate.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.Interfaces
{
   public interface IUser
    {
        IEnumerable<User> GetAllUser();
        UserVM GetUserByUserName(string userName);
        IEnumerable<UserVM> GetUsersByProject(Guid projectId);
        void CreateUser(User userToAdd);
        void UpdateUser(User userToUpdate);
        void DeleteUser(Guid userId);
       User CheckUser(string email, string password);
    }
}
