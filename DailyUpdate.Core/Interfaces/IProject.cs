﻿using DailyUpdate.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyUpdate.Core.Interfaces
{
   public interface IProject
    {
        IEnumerable<Project> GetProjectByProjectName(string project);
        IEnumerable<Project> GetAll();
        Project GetEntityById(Guid projectId);
        void UpdateEntity(Project entityToUpdate);
        void AddEntity(Project entityToAdd);
        void DeleteEntity(Guid projectId);
        Project GetProjectbyUser(Guid userId);
    }
}
